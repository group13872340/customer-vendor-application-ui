import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
@Component({
selector: 'app-customer',
  standalone: true,
  imports: [MatSelectModule,FormsModule],
  templateUrl: './customer.component.html',
  styleUrl: './customer.component.css'
})
export class CustomerComponent {
  private local:any;

  onSubmit(userform:any){
    this.local=userform;
    console.log(userform);
  }

}
