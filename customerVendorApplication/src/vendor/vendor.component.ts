import { Component } from '@angular/core';
import {MatSelectModule}from '@angular/material/select';
@Component({
  selector: 'app-vendor',
  standalone: true,
  imports: [MatSelectModule],
  templateUrl: './vendor.component.html',
  styleUrl: './vendor.component.css'
})
export class VendorComponent {

}
